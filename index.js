const Twitter = require('twitter-v2');
const { bearer_token } = require('./private/twitter.json');

const client = new Twitter({ bearer_token });

// list rules:
client.get('tweets/search/stream/rules').then((res) => {
  console.log(res);
});

// add rules:
// client.post('tweets/search/stream/rules', { add: [{ value: 'spacex has:images' }] }).then((res) => {
//   console.log('rule added', res);
// });

// delete rules:
// client.post('tweets/search/stream/rules', { delete: { ids: ['1523403552063991814'] } }).then((res) => {
//   console.log('delete res', res);
// });

async function main() {
  const stream = client.stream('tweets/search/stream');

  // Close the stream after 30 seconds
  setTimeout(() => {
    stream.close();
  }, 30 * 1000);

  // Read data from the stream
  for await (const { data } of stream) {
    console.log(`${data.id}: ${data.text.replace(/\s/g, ' ')}`);
  }

  console.log('Stream closed.');
}

main();
